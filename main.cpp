#include <iostream>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define GRAVITY 1
#define fps 60

bool init();
void close(std::vector<SDL_Texture*> textures, SDL_Renderer* renderer, SDL_Window* window);
void capFramerate(Uint32 startingTick);
SDL_Texture* loadTexture(std::string path);
class Object;

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
std::vector<SDL_Texture*> textures;

bool init()
{

	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		window = SDL_CreateWindow("Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (window == NULL)
		{
			std::cout << "Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if (renderer == NULL)
			{
				std::cout << "Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

				int imgFlags = IMG_INIT_PNG;
				if (!IMG_Init(imgFlags) & imgFlags)
				{
					std::cout << "SDL_image could ot initialize: " << IMG_GetError() << std::endl;
					success = false;
				}
			}
		}
	}

	return success;
}

void close(std::vector<SDL_Texture*> textures, SDL_Renderer* renderer, SDL_Window* window)
{
	for (int i = 0; i < textures.size(); i++)
	{
		SDL_DestroyTexture(textures[i]);
		textures[i] = NULL;
	}
	SDL_DestroyRenderer(renderer);
	renderer = NULL;
	SDL_DestroyWindow(window);
	window = NULL;
	IMG_Quit();
	SDL_Quit();
}

void capFramerate(Uint32 startingTick)
{
	if ((1000/fps) > SDL_GetTicks() - startingTick)
			{
				SDL_Delay(1000/fps - (SDL_GetTicks() - startingTick));
			}
}

SDL_Texture* loadTexture(std::string path)
{
	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		std::cout << "Error: " << IMG_GetError() << std::endl;
	}
	else
	{
		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (newTexture == NULL)
		{
			std::cout << "Error: " << IMG_GetError() << std::endl;
		}
		SDL_FreeSurface(loadedSurface);
	}
	return newTexture;
}

class Object
{
private:
	SDL_Surface* image;
	SDL_Texture* texture;
	SDL_Rect rect;
	SDL_Rect collRect;
	int velY = 0;
	int originX;
	int originY;

public:
	Object(std::string fileName, int x = 0, int y = 0, int w = SCREEN_WIDTH, int h = SCREEN_HEIGHT)
	{
		texture = loadTexture(fileName.c_str());
		textures.push_back(texture);
		originX = x;
		originY = y;

		if (texture == NULL)
			{
				std::cout << "Unable to load image: " << SDL_GetError() << std::endl << IMG_GetError() << std::endl;
			}

		rect.x = x;
		rect.y = y;
		rect.w = w;
		rect.h = h;

		collRect.x = x + 10;
		collRect.y = y + 10;
		collRect.w = w - 10;
		collRect.h = h - 20;
		/*easier way: modify collision function with margins*/
	}

	void fall()
	{
		velY += GRAVITY;
		if (rect.y >= originY)
		{
			velY = 0;
			rect.y = originY;
			collRect.y = rect.y;
		}
		else
		{
			rect.y += velY;
			collRect.y = rect.y;
		}
	}

	void jump()
	{
		if (rect.y == originY)
		{
			//std::cout << "ground" << std::endl;	
			velY = -20;
			rect.y += velY;
			collRect.y = rect.y;
		}
		/*else { std::cout << "air" << std::endl; }*/
	}

	void slide()
	{
		rect.x -= 10;
		if (originX - rect.x >= SCREEN_WIDTH)
		{
			rect.x = originX;
		}
		collRect.x = rect.x;
	}

	SDL_Rect getCollRect()
	{
		return collRect;
	}

	bool checkCollision(SDL_Rect cactusRect)
	{
		int leftA, leftB;
	    	int rightA, rightB;
	    	int topA, topB;
	    	int bottomA, bottomB;

	    	leftA = collRect.x;
	    	rightA = collRect.x + collRect.w;
	    	topA = collRect.y;
	    	bottomA = collRect.y + collRect.h;

	    	leftB = cactusRect.x;
	    	rightB = cactusRect.x + cactusRect.w;
	    	topB = cactusRect.y;
	    	bottomB = cactusRect.y + cactusRect.h;

		if (bottomA <= topB)
		{
			return false;
		}

		if (topA >= bottomB)
		{
			return false;
		}

		if (rightA <= leftB)
		{
			return false;
		}

		if (leftA >= rightB)
		{
			return false;
		}
		return true;
	}

	void draw()
	{
		SDL_RenderCopy(renderer, texture, NULL, &rect);
	}

};

int main(int argc, char* args[])
{
	if (!init())
	{
		std::cout << "Failed to initialize" << std::endl;
	}
	else
	{
		bool running = true;
		SDL_Event event;
		Uint32 startingTick;
		Uint32 counter = 0;

		Object background("landscape.png");
		Object hero("pacman.png", 10, 433, 120, 120);
		Object ground("base.png", 0, 542, 800, 200); 
		Object ground2("base.png", SCREEN_WIDTH, 542, SCREEN_WIDTH, 200);
		Object cactus("cactus.png", 0, 422, 100, 130);
		Object cactus2("cactus.png", SCREEN_WIDTH, 422, 100, 130);
		
		while (running) 
		{
			startingTick = SDL_GetTicks();

			while (SDL_PollEvent(&event)) 
			{
				if (event.type == SDL_QUIT) 	
				{
					running = false;
					break;
				}

				else if (event.type == SDL_KEYDOWN)
				{
					int keyDir = event.key.keysym.sym; 
					if (keyDir == SDLK_SPACE)
					{
						hero.jump();
					}
				}
			}
			capFramerate(startingTick);
			SDL_RenderClear(renderer);
			hero.fall();
			ground.slide();
			ground2.slide();
			cactus.slide();
			cactus2.slide();
			background.draw();
			counter++;

			if (counter < 12)
			{
				hero.draw();
				ground.draw();
				ground2.draw();
			}
			else if (counter < 30)
			{
				cactus.draw();
				cactus2.draw();
				hero.draw();
				ground.draw();
				ground2.draw();
			}
			else
			{
				cactus.draw();
				cactus2.draw();
				hero.draw();
				ground.draw();
				ground2.draw();
				SDL_Rect cactusRect = cactus.getCollRect();
				SDL_Rect cactus2Rect = cactus2.getCollRect();
				if (hero.checkCollision(cactusRect) || hero.checkCollision(cactus2Rect))
				{
					std::cout << "boom" << std::endl;
				}
			}
			SDL_RenderPresent(renderer);
		}
		close(textures, renderer, window);
	}
	return 0;
}





